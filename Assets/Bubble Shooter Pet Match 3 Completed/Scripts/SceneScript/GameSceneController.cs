﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;
using Prime31;

public class GameSceneController : MonoBehaviour
{

	GameplayController gamePlayController;

	public GameObject FadeObject;
	public GameObject AdButton;

	public static bool needShowFade = false;
	#if UNITY_ANDROID
	private string zoneID = "vz8ad20771a040471693";
	#else
	private string zoneID = "vzfaa5e6037d1b42f8ae";
	#endif

	private TDGAAccount account;

	void Start ()
	{
		needShowFade = false;
		FadeObject.GetComponent<Animation> ().Play ("FadeIn");
		gamePlayController = GameObject.FindObjectOfType<GameplayController> ();

		// Initial
		// Levelselect switch
		LevelSelectController.NewUnlockedLevel = -1;
		LevelSelectController.needUpdateMoveSquirrelIcon = false;

		//Adconoly Ads
		#if UNITY_ANDROID
		AdColony.Configure
		(
			"version:1.0,store:google", // Arbitrary app version and Android app store declaration.
			"appf396fb75662a4115b6",   // ADC App ID from adcolony.com
			"vz8ad20771a040471693" // A zone ID from adcolony.com
		);
		#else 
		AdColony.Configure
		(
			"version:1.0,store:apple", // Arbitrary app version and Android app store declaration.
			"appe44ff2f410744ef5a4",   // ADC App ID from adcolony.com
			"vzfaa5e6037d1b42f8ae" // A zone ID from adcolony.com
		);
		#endif
		//Unity Ads
//		if (Advertisement.isSupported) {
//#if UNITY_ANDROID
//			if (Application.platform == RuntimePlatform.Android) {
//				Advertisement.Initialize ("1020654", false);
//			} else {
//				Advertisement.Initialize ("1020654", true);
//			}
//#elif UNITY_IOS
//			Advertisement.Initialize("1020655",true);
//#endif
//		}

		// Enable advertisement banner
		AdMob.hideBanner (false);

	}

	void Update ()
	{
		if (needShowFade) {
			needShowFade = false;
			FadeObject.GetComponent<Animation> ().Play ("FadeOut");
		}
	}

	// Scene Controlling
	// Menu Popup Area
	public GameObject PauseMenu, WinMenu, EndlessWinMenu, LoseMenu, OptionMenu;
	public UnityEngine.UI.Button PauseButton;
	public TextMesh bubbleText;

	public void PauseAndAwardPlayer ()
	{

//		if (Advertisement.IsReady ("rewardedVideo")) {
//			ShowOptions options = new ShowOptions ();
//			options.resultCallback = HandleShowResult;
//			if (!gamePlayController.gamePaused) {
//				PauseGame ();
//			}
////			options.pause = true;
////			FadeObject.GetComponent<CanvasGroup> ().blocksRaycasts = true;
////			AudioListener.volume = 0;
////			Advertisement.Show ("rewardedVideo", options);
//		}
		if (AdColony.IsVideoAvailable (zoneID)) {
//			ShowOptions options = new ShowOptions ();
//			options.resultCallback = HandleShowResult;
			if (!gamePlayController.gamePaused) {
				PauseGame ();
			}
			Debug.Log ("Play AdColony Video");
			// Call AdColony.ShowVideoAd with that zone to play an interstitial video.
			// Note that you should also pause your game here (audio, etc.) AdColony will not
			// pause your app for you.
			AdColony.ShowVideoAd (zoneID);
			gamePlayController.RemainBubbleShoot += 3;
		} else {
			Debug.Log ("Video Not Available");
		}
	}

	//Unity Advertisement
	//	private void HandleShowResult (ShowResult result)
	//	{
	//		Debug.Log ("Callback! " + result);
	//		switch (result) {
	//		case (ShowResult.Failed):
	//			Debug.LogError ("Failed to load Ad");
	//			break;
	//		case (ShowResult.Finished):
	//			gamePlayController.RemainBubbleShoot += 3;
	//			Debug.Log ("Reward");
	//			break;
	//		case (ShowResult.Skipped):
	//			Debug.Log ("Ad is skipped, no reward");
	//			break;
	//		}
	////		AudioListener.volume = 1;
	////		FadeObject.GetComponent<CanvasGroup> ().blocksRaycasts = true;
	//	}

	public void PauseGame ()
	{
		if (gamePlayController.gamePaused) {
			gamePlayController.gamePaused = false;
			PauseMenu.gameObject.SetActive (false);
			PauseButton.enabled = true;
		} else {
			gamePlayController.gamePaused = true;
			PauseMenu.gameObject.SetActive (true);
			PauseButton.enabled = false;
		}
	}

	public void OpenSetting ()
	{
		PauseMenu.gameObject.SetActive (false);
		OptionMenu.gameObject.SetActive (true);
	}

	public void CloseSetting ()
	{
		PauseMenu.gameObject.SetActive (true);
		OptionMenu.gameObject.SetActive (false);
	}

	public void RestartLevel ()
	{
		Application.LoadLevel ("GameScene");
	}

	public void ReviewGame ()
	{
		Application.OpenURL ("https://play.google.com/store/apps/details?id=com.divinegame.bubbleadventurepre");
	}

	public void NextLevel ()
	{
		if (GlobalData.gameMode == GlobalData.GameMode.PUZZLE_MODE) {
			LevelSelectController.ShowPlayDialog (GlobalData.GetCurrentLevel () + 1);
			account = TDGAAccount.SetAccount (TalkingDataGA.GetDeviceId ());
			account.SetLevel (GlobalData.GetCurrentLevel ());
			GoToHomeScene ();
		} else if (GlobalData.gameMode == GlobalData.GameMode.ENDLESS_MODE) {
			// Continue gameplay
			EndlessWinMenu.SetActive (false);
			gamePlayController.ContinuePlayEndless ();
		}
	}

	public void GoToHomeScene ()
	{
		GameSceneController.needShowFade = true;

		// Reset load counter
		BranchPairSetup.ResetAllValue ();

		if (GlobalData.gameMode == GlobalData.GameMode.PUZZLE_MODE) {
			Application.LoadLevel ("LevelSelect");
		} else if (GlobalData.gameMode == GlobalData.GameMode.ENDLESS_MODE) {
			Application.LoadLevel ("HomeScene");
		}
	}

}
