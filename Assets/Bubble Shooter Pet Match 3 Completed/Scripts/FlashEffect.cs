﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using DG.Tweening;

public class FlashEffect : MonoBehaviour
{
	[SerializeField] float m_FlashInterval = 5f;
	[SerializeField] Image m_FlashComponent;

	Tweener m_FlashingTween1;
	Tweener m_FlashingTween2;


	public void StartTween ()
	{
		TweenAnim (m_FlashInterval);
	}

	void TweenAnim (float interval = 5f)
	{
		m_FlashComponent.color = Color.white;
		m_FlashComponent.transform.localScale = Vector3.one * 0.02f;
		m_FlashingTween1 = m_FlashComponent.DOFade (0, 0.5f).SetDelay (interval).OnComplete (() => {
			m_FlashingTween1 = null;
		});
		m_FlashingTween2 = m_FlashComponent.transform.DOScale (0.025f, 0.5f).SetDelay (interval).OnComplete (() => {
			m_FlashingTween2 = null;
			TweenAnim (interval);
		});
	}

	public void StopTween ()
	{
		if (m_FlashingTween1 != null) {
			m_FlashingTween1.Kill ();
			m_FlashingTween1 = null;
		}
		if (m_FlashingTween2 != null) {
			m_FlashingTween2.Kill ();
			m_FlashingTween2 = null;
		}
		m_FlashComponent.transform.localScale = Vector3.zero;
	}
}
