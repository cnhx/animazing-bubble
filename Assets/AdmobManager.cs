﻿using UnityEngine;
using System.Collections;
using Prime31;

public class AdmobManager : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		AdMob.setTestDevices (new string[] {
			"6D13FA054BC989C5AC41900EE14B0C1B",
			"8E2F04DC5B964AFD3BC2D90396A9DA6E",
			"3BAB93112BBB08713B6D6D0A09EDABA1",
			"079adeed23ef3e9a9ddf0f10c92b8e18",
			"E2236E5E84CD318D4AD96B62B6E0EE2B",
			"149c34313ce10e43812233aad0b9aa4d",
			"d1eb708cec2ca65c9f95458b22fbdc95",
			"1908bb1db3ae636e14cd6cc08cd5cb7a"
		});
		AdMob.createBanner ("ca-app-pub-4509495752354188/3615632154", "ca-app-pub-4509495752354188/9662165758", AdMobBanner.SmartBanner, AdMobLocation.BottomCenter);
		AdMob.hideBanner (false);
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnEnable ()
	{
		AdMob.hideBanner (false);
	}

	void OnDisable ()
	{
		AdMob.hideBanner (true);
	}

}
