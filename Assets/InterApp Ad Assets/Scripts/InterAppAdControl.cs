﻿using UnityEngine;
using UnityEngine.UI;
using Parse;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;

public class InterAppAdControl : MonoBehaviour
{

	public float fps = 0.25f;
	public string key;

	public Texture2D[] defaultAds;
	public string[] defaultOpenUrls;

	public Canvas adCanvas;
	public RawImage image;

	Texture2D[] ads;
	string[] openUrls;
	int pointer;
	bool hasInternet;
	bool pause;

	// Parse initializer
	private string applicationID = "qpjbcSCxO1r1y0gOf3DBKqhgJvXw5no5VYFrip1h";
	private string dotnetKey = "0ORv3XWnFTgtcVGe03DkRYElDTvD67eZKzSM9irJ";

	// Local storage path
	private string localBasePath;


	void Awake ()
	{
		adCanvas.enabled = false;
		hasInternet = false;
		pause = false;
//		DontDestroyOnLoad(gameObject);
		#if UNITY_EDITOR
		localBasePath = Application.dataPath + "/../";
		#else
		localBasePath = Application.persistentDataPath + "/";
		#endif

		// Add ParseInitializeBehaviour
		GameObject parse = new GameObject ("ParseInitializeBehaviour");
		DontDestroyOnLoad (parse);
		parse.SetActive (false);
		ParseInitializeBehaviour parseInitializeBehaviour = parse.AddComponent<ParseInitializeBehaviour> ();
		parseInitializeBehaviour.applicationID = applicationID;
		parseInitializeBehaviour.dotnetKey = dotnetKey;
		parse.SetActive (true);
	}

	IEnumerator Start ()
	{
		Ping ping = new Ping ("8.8.8.8");
		float startTime = Time.time;
		while (!ping.isDone && Time.time < startTime + 5.0f) {
			yield return new WaitForSeconds (0.1f);
		}

		if (ping.isDone) {
			AdsInit ();
		} else {
			hasInternet = false;
			Debug.LogWarning ("No internet support");
			AdsDefaultInit ();
		}
	}

	public void AdsInit ()
	{
		if (string.IsNullOrEmpty (key))
			return;
		StartCoroutine ("GetAds");
	}

	public void AdsDefaultInit ()
	{
		ads = defaultAds;
		openUrls = defaultOpenUrls;
		image.texture = ads [0];
		adCanvas.enabled = true;
		hasInternet = true;
	}

	IEnumerator GetAds ()
	{

		//Check version
		WWW _v = new WWW ("http://interappad.parseapp.com/versions/" + key + ".txt");
		yield return _v;
		if (!string.IsNullOrEmpty (_v.error)) {
			Debug.LogError (_v.error);
			AdsDefaultInit ();
			yield break;
		}
		int version = int.Parse (_v.text);
		Debug.Log ("Ads Version: " + version);
		int currentVersion = -1;
		if (PlayerPrefs.HasKey ("_v_"))
			currentVersion = PlayerPrefs.GetInt ("_v_");
		if (version == currentVersion) {
			//load local image
			Debug.Log ("Cached ads");
			int numOfAds = PlayerPrefs.GetInt ("_num_");
			ads = new Texture2D[numOfAds];
			openUrls = new string[numOfAds];
			for (int i = 0; i < numOfAds; i++) {
				WWW _file = new WWW ("file://" + localBasePath + "ads/" + "_" + i.ToString () + ".png");
				yield return _file;
				ads [i] = _file.texture;
				openUrls [i] = PlayerPrefs.GetString ("_url_" + i.ToString ());
			}
			ShowAds ();
			yield break;
		}

		// version is out of date or no local version stored
		ParseQuery<ParseObject> query = ParseObject.GetQuery ("advertisement")
			.WhereEqualTo ("key", key);
		var task = query.FindAsync ();

		while (!task.IsCompleted) {
			yield return null;
		}
		if (task.IsFaulted || task.IsCanceled) {
			Debug.LogError (task.Exception.Message);
			AdsDefaultInit ();
			yield break;
		}
		var result = task.Result;
		List<ParseObject> resultList = new List<ParseObject> ();
		foreach (ParseObject obj in result) {
			resultList.Add (obj);
		}

		//check if directory exists
		if (!Directory.Exists (localBasePath + "ads/")) {
			Directory.CreateDirectory (localBasePath + "ads/");
		}

		ads = new Texture2D[resultList.Count];
		openUrls = new string[resultList.Count];
		bool dataIntegrity = true;
		for (int i = 0; i < resultList.Count; i++) {
			string url = resultList [i].Get<string> ("url");
			string open = resultList [i].Get<string> ("openurl");
			WWW www = new WWW (url);
			yield return www;
			if (string.IsNullOrEmpty (www.error)) {
				ads [i] = www.texture;
				openUrls [i] = open;
			} else {
				Debug.LogError ("Cannot load ads from " + url);
				dataIntegrity = false;
				ads [i] = defaultAds [i % defaultAds.Length];
				openUrls [i] = defaultOpenUrls [i % defaultOpenUrls.Length];
			}
		}
		if (dataIntegrity) {
			// Save ads to local storage
			Debug.Log ("Caching ads");
			PlayerPrefs.SetInt ("_v_", version);
			PlayerPrefs.SetInt ("_num_", ads.Length);
			for (int i = 0; i < ads.Length; i++) {
				var file = File.Open (localBasePath + "ads/" + "_" + i.ToString () + ".png", FileMode.OpenOrCreate);
				var binary = new BinaryWriter (file);
				binary.Write (ads [i].EncodeToPNG ());
				file.Close ();
				PlayerPrefs.SetString ("_url_" + i.ToString (), openUrls [i]);
			}
		}
		ShowAds ();
	}

	void ShowAds ()
	{
		image.texture = ads [0];
		adCanvas.enabled = true;
		hasInternet = true;
	}

	void Update ()
	{
		if (hasInternet && !pause) {
			int index = Mathf.CeilToInt (Time.time * fps);
			index %= ads.Length;
			image.texture = ads [index];
			pointer = index;
		}
	}

	#region Button Messages

	public void OpenURL ()
	{
		Debug.Log ("111");
		pause = true;
		Application.OpenURL (openUrls [pointer]);
		pause = false;
	}

	#endregion
}