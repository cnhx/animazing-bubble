﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;

public class TDManager : MonoBehaviour
{

	int index = 1;
	int level = 1;
	TDGAAccount account;

	// Use this for initialization
	void Start ()
	{
		DontDestroyOnLoad (this.gameObject);
		#if UNITY_ANDROID
		TalkingDataGA.OnStart ("C724F5ACBEB2D3C24E7EAD20A59902EC", "Google Play");
		#else 
		TalkingDataGA.OnStart ("C724F5ACBEB2D3C24E7EAD20A59902EC", "iOS");
		#endif
		account = TDGAAccount.SetAccount (TalkingDataGA.GetDeviceId ());
		Debug.Log ("TalkingData Initialised");
	}

	// Update is called once per frame
	void Update ()
	{

	}

	void OnDestroy ()
	{
		TalkingDataGA.OnEnd ();
		Debug.Log ("onDestroy");
	}

	void Awake ()
	{
		Debug.Log ("Awake");
	}

	void OnEnable ()
	{
		Debug.Log ("OnEnable");
	}

	void OnDisable ()
	{
		Debug.Log ("OnDisable");
	}
}
